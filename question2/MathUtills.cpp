#include "MathUtills.h"
#include "Math.h"
double MathUtills::CalPentagonArea(double length)
{
	double area = 0;
	area = 1.72048 * pow(length, 2);
	return area;
}

double MathUtills::CalHexagonArea(double length)
{
	double area = 2.59808 * pow(length,2);
	return area;
}