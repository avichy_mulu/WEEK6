#pragma once
#include "Shape.h"
class pentagon : public Shape
{
public:
	pentagon(std::string name, std::string color, double length);
	~pentagon();
	void draw();
	void setLength(const double);
private:
	double length;
};

