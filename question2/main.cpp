#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "hexagon.h"
#include "pentagon.h"
#include "parallelogram.h"
#include "inputException.h"
#include <string>
#include "shapeException.h"

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0;double length = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	hexagon hexa(nam, col,length);
	pentagon penta(nam, col, length);
	parallelogram para(nam, col, width, height, ang, ang2);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape* ptrhexa = &hexa;
	Shape* ptrpenta = &penta;

	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; std::string shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = hexagon , 6 = pentagon" << std::endl;
		std::getline(std::cin, shapetype);
		try
		{
			if(shapetype.length()>1)// to check if the user wrote more than one letter
			{
				std::cout <<"Warning - Don't try to build more than one shape at once\n";
			}
			if (shapetype[0] == 'c')
			{
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
			}
			else if (shapetype[0] == 'q')
			{
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
			}
			else if (shapetype[0] == 'r')
			{
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
			}
			else if (shapetype[0] == 'p')
			{
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (ang == ang2 || ang>180 || ang2>180 || ang <0 || ang2<0)
				{
					throw shapeException();
				}
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
			}
			else if (shapetype[0] == 'h')
			{
				std::cout << "enter name, color, length" << std::endl;
				std::cin >> nam >> col >> length;

				hexa.setName(nam);
				hexa.setColor(col);
				hexa.setLength(length);
				ptrhexa->draw();	
			}
			else if (shapetype[0] == '6')
			{
				std::cout << "enter name, color, length" << std::endl;
				std::cin >> nam >> col >> length;

				penta.setName(nam);
				penta.setColor(col);
				penta.setLength(length);
				ptrpenta->draw();
			}
			else
			{
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
			}
			std::cout << "\nwould you like to add more object press any key if not press x" << std::endl;
			std::cin>> x;
			getchar();
		}
		catch (InputException x)
		{
			printf(x.what());
		}
		catch (shapeException e)
		{			
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}