#pragma once
#include "Shape.h"
class hexagon : public Shape
{
public:
	hexagon(std::string name,std::string color,double length);
	~hexagon();
	void draw();
	void setLength(const double);
private:
	double length;
};
