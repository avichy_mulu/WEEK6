#include "pentagon.h"
#include "MathUtills.h"
//#include "MathUtills.cpp"
pentagon::pentagon(std::string name, std::string color, double length) : Shape(name =name, color=color)
{
	this->length = length;
}

void pentagon::draw()
{
	std::cout << "Name: "<<this->getName() << " \nColor: " << this->getColor() << "\nArea: " << MathUtills::CalPentagonArea(length);
}

pentagon::~pentagon() {}

void pentagon::setLength(const double length)
{
	this->length = length;
}