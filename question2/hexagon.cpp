#include "hexagon.h"
#include "MathUtills.h"
hexagon::hexagon(std::string name, std::string color, double length) : Shape(name = name, color = color)
{
	this->length = length;
}

void hexagon::draw()
{
	std::cout << "Name: " << this->getName() << " \nColor: " << this->getColor() << "\nArea: " << MathUtills::CalHexagonArea(length);
}
hexagon::~hexagon()
{
}
void hexagon::setLength(const double length)
{
	this->length = length;
}