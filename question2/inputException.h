#pragma once
#include <exception>


class InputException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "ERROR -- You did not enter an integer\n";
	}
};
