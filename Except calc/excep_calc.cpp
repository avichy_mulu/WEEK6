#include <iostream>
std::string errorString = "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year";
int add(int a, int b) {
	if (a + b == 8200)
		throw std::string(errorString);
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
		if (sum == 8200)
			throw std::string(errorString);
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
		if (exponent == 8200)
			throw std::string(errorString);
	};
	return exponent;
}

int main(void) {
	try
	{
		std::cout << multiply(4100,2) << std::endl;
	}
	catch (const std::string errorString)
	{
		std::cerr << "Error: " << errorString;
	}
}