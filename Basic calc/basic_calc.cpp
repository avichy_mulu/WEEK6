#include <iostream>
bool is_8200 = false;
int add(int a, int b) {
	if (a + b == 8200)
		is_8200 = true;
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
		if (sum == 8200)
			is_8200 = true;
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
		if (exponent == 8200)
			is_8200 = true;
	};
	return exponent;
}

int main(void) {
	int x=0, y = 0,result=0;
	//bool is_8200 = false;
	std::cout << "Enter a number";
	std::cin >> x;
	std::cout << "Enter a number";
	std::cin >> y;
	result = multiply(x, y);
	if (is_8200)
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year";
	else
		std::cout << result << std::endl;
}